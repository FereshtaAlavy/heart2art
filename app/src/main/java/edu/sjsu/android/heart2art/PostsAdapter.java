package edu.sjsu.android.heart2art;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parse.ParseFile;

import java.util.List;


public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {
    private Context context;
    private List<Post> posts;

    /**
     * Constructor that sets context and compiled posts of a user
     * @param context
     * @param posts
     */
    public PostsAdapter(Context context, List<Post> posts){
        this.context = context;
        this.posts = posts;
    }

    /**
     * Method that constructs a a new view to hold the posts for user profile
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.obj_post, parent, false);
        return new ViewHolder(view);
    }

    /**
     * Method that binds the view holder to the specified post
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull PostsAdapter.ViewHolder holder, int position) {
        Post post= posts.get(position);
        holder.bind(post);
    }

    /**
     * Method that returns the amount of posts a user has
     * @return
     */
    @Override
    public int getItemCount() {
        return posts.size();
    }

    /**
     * Method that clears all the posts from a user
     */
    public void clear() {
        posts.clear();
        notifyDataSetChanged();
    }

    /**
     * Method that adds all the posts to a user's posts
     * @param post
     */
    public void addAll(List<Post> post) {
        posts.addAll(post);
        notifyDataSetChanged();
    }

    /**
     * ViewHolder class that
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView username;
        private ImageView postedPic;
        private TextView description;
        private ImageView comment;
        private ImageView like;
        private ImageView share;

        /**
         * Class to set view for viewing Posts
         * @param itemView
         */
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.post_username);
            postedPic = itemView.findViewById(R.id.posted_picture);
            description = itemView.findViewById(R.id.post_description);
            comment = itemView.findViewById(R.id.post_comment);
            like = itemView.findViewById(R.id.post_like);
            share = itemView.findViewById(R.id.post_share);
        }

        public void bind(Post post){
            username.setText(post.getUser().getUsername());
            ParseFile image = post.getImage();
            if(image != null) {
                Log.d("value is", "I'm printing a goddamn string");
                Glide.with(context).load(image.getUrl()).into(postedPic);
            }
            description.setText(post.getDescription());
        }
    }

}
