package edu.sjsu.android.heart2art;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SignUpActivity extends AppCompatActivity {
    private static String TAG = "SignUpActivity";
    private EditText etEmail;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnSignUp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        etEmail = findViewById(R.id.etEmail);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnSignUp = findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                if (email.length()== 0 || username.length() == 0 || password.length() == 0){
                    Toast.makeText(SignUpActivity.this, "Please enter the above information", Toast.LENGTH_LONG).show();
                    return;
                }

                SignUp(email, username,password);

                //Toast.makeText(SignUpActivity.this, "You have signed up successfully", Toast.LENGTH_LONG).show();

            }
        });


    }

    private void SignUp(String email, String username, String password) {
        Intent intent = new Intent(getBaseContext(), SignUpUserInfoActivity.class);

        intent.putExtra("USER_EMAIL", email);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("USER_PASSWORD", password);

        etEmail.setText("");
        etUsername.setText("");
        etPassword.setText("");

        startActivity(intent);
    }
}
