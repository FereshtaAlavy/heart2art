package edu.sjsu.android.heart2art;

import android.content.Context;
import java.util.ArrayList;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View{

    public static int BRUSH_SIZE = 20;
    public static final int DEFAULT_BRUSH_COLOR = Color.parseColor("#1AA19C");  // Teal brush color
    public static final int DEFAULT_BACKGROUND_COLOR = Color.parseColor("#FCFCFC"); // Offwhite Background color
    private static final float TOUCH_TOLERANCE = 4;
    private float uX, uY;   // User's coordinates
    private Path uPath;     // User's path
    private Paint uPaint;   // User's paint object
    private ArrayList<FingerDraw> paths = new ArrayList<FingerDraw>();  // Groups every user's path/line of drawing to create a "picture"
    private int currColor;  // Color that user currently uses - option to change color
    private int bgColor = DEFAULT_BACKGROUND_COLOR; // Current background color
    private int strokeWidth;
    private boolean embossEffect;
    private boolean blurEffect;
    private MaskFilter uEmboss; // Filter used to create the Emboss effect on user's drawing if stampEffect is true
    private MaskFilter uBlur; // Filter used to create the Blur effect on user's drawing if blurEffect is true
    private Bitmap uBitMap;
    private Canvas uCanvas;
    private Paint uBitMapPaint = new Paint(Paint.DITHER_FLAG);  // Paint flag that enables dithering when blitting.

    public DrawView(Context context){
        this(context, null);
        bgColor = DEFAULT_BACKGROUND_COLOR;
    }

    public DrawView(Context context, AttributeSet attributes){
        super(context, attributes);
        uPaint = new Paint();
        uPaint.setAntiAlias(true);
        uPaint.setDither(true);
        uPaint.setColor(DEFAULT_BRUSH_COLOR);
        uPaint.setStyle(Paint.Style.STROKE);
        uPaint.setStrokeJoin(Paint.Join.ROUND);
        uPaint.setStrokeCap(Paint.Cap.ROUND);
        uPaint.setXfermode(null);
        uPaint.setAlpha(0xff);

        uEmboss = new EmbossMaskFilter(new float[] {1, 1, 1}, 0.4f, 6, 3.5f);
        uBlur = new BlurMaskFilter(5, BlurMaskFilter.Blur.NORMAL);
        bgColor = DEFAULT_BACKGROUND_COLOR;
    }

    public void init(DisplayMetrics metrics){
        uBitMap = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888);
        uCanvas = new Canvas(uBitMap);

        currColor = DEFAULT_BRUSH_COLOR;
        strokeWidth = BRUSH_SIZE;
    }

    public void normal(){
        embossEffect = false;
        blurEffect = false;
    }

    public void embossEffect(){
        embossEffect = true;
        blurEffect = false;
    }

    public void blurEffect(){
        embossEffect = false;
        blurEffect = true;
    }

    public void clear() {
        bgColor = DEFAULT_BACKGROUND_COLOR;
        paths.clear();
        normal();
        invalidate();
    }

    public void onDraw(Canvas canvas){
        canvas.save();
        uCanvas.drawColor(bgColor);

        for(FingerDraw fd: paths){
            uPaint.setColor(fd.color);
            uPaint.setStrokeWidth(fd.strokeWidth);
            uPaint.setMaskFilter(null);

            if(fd.embossEffect){
                uPaint.setMaskFilter(uEmboss);
            }
            else if (fd.blurEffect){
                uPaint.setMaskFilter(uBlur);
            }
            uCanvas.drawPath(fd.path, uPaint);
        }
        canvas.drawBitmap(uBitMap, 0, 0, uBitMapPaint);
        canvas.restore();
    }

    private void touchStart(float x, float y) {
        uPath = new Path();
        FingerDraw fd = new FingerDraw(currColor, embossEffect, blurEffect, strokeWidth, uPath);
        paths.add(fd);
        uPath.reset();
        uPath.moveTo(x, y);
        uX = x;
        uY = y;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - uX);
        float dy = Math.abs(y - uY);

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            uPath.quadTo(uX, uY, (x + uX) / 2, (y + uY) / 2);
            uX = x;
            uY = y;
        }
    }

    private void touchUp() {
        uPath.lineTo(uX, uY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eX = event.getX();
        float eY = event.getY();

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN :
                touchStart(eX, eY);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE :
                touchMove(eX, eY);
                invalidate();
                break;
            case MotionEvent.ACTION_UP :
                touchUp();
                invalidate();
                break;
        }
        return true;
    }

}

















