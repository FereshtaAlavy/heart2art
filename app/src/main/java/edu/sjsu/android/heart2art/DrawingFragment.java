package edu.sjsu.android.heart2art;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.raed.drawingview.BrushView;
import com.raed.drawingview.DrawingView;
import com.raed.drawingview.brushes.BrushSettings;
import com.raed.drawingview.brushes.Brushes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import yuku.ambilwarna.AmbilWarnaDialog;

public class DrawingFragment extends Fragment  implements View.OnClickListener,View.OnTouchListener {
    private static final String TAG = "DrawingFragment";
    public final static int PICK_PHOTO_CODE = 1046;
    int mDefaultColor;
    boolean erase=false;

    DrawingView drawingView;
    ImageButton  brushbtn,uploadPhotoBtn, savebtn;
    Button brushsmallbtn, brushmediumbtn, brushlargebtn;
    EditText etDescription;
    Button colorbtn;
    private View view;
    public Post post;
    public String id;
    public File photoFile;
    BrushSettings brushSettings;
    BrushView brushView;
    Paint drawPaint;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_draw, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        brushbtn = (ImageButton) view.findViewById(R.id.brushbtn);
        savebtn = (ImageButton) view.findViewById(R.id.save_btn);
        colorbtn = view.findViewById(R.id.colorbtn);
        uploadPhotoBtn = (ImageButton) view.findViewById(R.id.photo_insert_btn);
        etDescription = view.findViewById(R.id.description);
        brushbtn.setOnClickListener(this);
        uploadPhotoBtn.setOnClickListener(this);

        savebtn.setOnClickListener(this);
        drawingView = view.findViewById(R.id.draw);
        brushView = view.findViewById(R.id.brush_view);
        brushView.setDrawingView(drawingView);
        brushSettings = drawingView.getBrushSettings();

        colorbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openColorPicker();
            }
        });
    }
    public void setErase(boolean isErase){
        erase=isErase;
        if(erase) drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        else drawPaint.setXfermode(null);
    }

    private void openColorPicker() {

        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(getContext(), mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                mDefaultColor = color;
                brushSettings.setColor(mDefaultColor);
            }
        });
        colorPicker.show();
    }


    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.brushbtn) {
            final Dialog brushDialog = new Dialog(getContext());
            brushDialog.setTitle("choose size");
            brushDialog.setContentView(R.layout.brush_chooser);
            brushsmallbtn = brushDialog.findViewById(R.id.small_brush);
            brushmediumbtn = brushDialog.findViewById(R.id.medium_brush);
            brushlargebtn = brushDialog.findViewById(R.id.large_brush);
            brushsmallbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    brushSettings.setBrushSize(Brushes.PENCIL, 0.2f);
                    brushDialog.dismiss();
                }
            });

            brushmediumbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   brushSettings.setBrushSize(Brushes.PENCIL, 0.5f);
                   brushDialog.dismiss();
                }
            });

            brushlargebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    brushSettings.setBrushSize(Brushes.PENCIL, 1.0f);
                    brushDialog.dismiss();
                }
            });
            brushDialog.show();

        }else if(view.getId() == R.id.photo_insert_btn){
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Bring up gallery to select a photo
                startActivityForResult(intent, PICK_PHOTO_CODE);
            }

        }else if(view.getId() == R.id.save_btn){

            AlertDialog.Builder saveDialog = new AlertDialog.Builder(getContext());
            saveDialog.setTitle("Would you like to save the picture?");
            saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Bitmap bitmap = drawingView.exportDrawing();
                    photoFile = convertBitmapToFile(bitmap);
                    String description = etDescription.getText().toString();
                    ParseUser user = ParseUser.getCurrentUser();
                    if (photoFile == null) {
                        Toast.makeText(getContext(), "There is no photo", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        saveImage(description,user,photoFile);
                        Toast.makeText(getContext(), "Photo is saved", Toast.LENGTH_SHORT).show();
                    }


                }
            });

            saveDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });

            saveDialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            Uri photoUri = data.getData();
            Bitmap photo = null;
            try {
                photo = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            drawingView.setBackgroundImage(photo);

        }
    }

    private File convertBitmapToFile(Bitmap bitmap) {
        File f = new File(getContext().getCacheDir(), "draw.png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    private void saveImage(String des, ParseUser user, File file) {
        post = new Post();
        post.setImage(new ParseFile(file));
        post.setDescription(des);
        post.setUser(user);

        post.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                id = post.getObjectId();
                if(e != null){
                    e.printStackTrace();
                    return;
                }
                etDescription.setText("");
                drawingView.setBackgroundImage(null);
                drawingView.clear();
            }
        });
    }



    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}

