package edu.sjsu.android.heart2art;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SignUpUserInfoActivity extends AppCompatActivity {
    public final static int PICK_PHOTO_CODE = 1046;
    private static String TAG = "SignUpActivity";
    private EditText edit_user_title;
    private EditText et_user_bio;
    private EditText et_user_location;
    private Button btnSignUp;
    String username;
    String email;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        username = getIntent().getStringExtra("USER_NAME");
        email = getIntent().getStringExtra("USER_EMAIL");
        password = getIntent().getStringExtra("USER_PASSWORD");

        edit_user_title = findViewById(R.id.edit_user_title);
        et_user_bio = findViewById(R.id.et_user_bio);
        et_user_location = findViewById(R.id.et_user_location);
        btnSignUp = findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = edit_user_title.getText().toString();
                String bio = et_user_bio.getText().toString();
                String location = et_user_location.getText().toString();

                if (title.length()== 0 || bio.length() == 0 || location.length() == 0){
                    Toast.makeText(SignUpUserInfoActivity.this, "Please enter the above information", Toast.LENGTH_LONG).show();
                    return;
                }
                DeclareMe(title, bio, location);

                Toast.makeText(SignUpUserInfoActivity.this, "You have signed up successfully", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);

            }
        });
    }

    private void DeclareMe(String title, String bio, String location) {

        ParseUser user = new ParseUser();
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(password);
        user.put("bio", bio);
        user.put("user_title", title);
        user.put("user_location", location);

        user.signUpInBackground(new SignUpCallback() {
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            // Hooray! Let them use the app now.
                                        } else {
                                            // Sign up didn't succeed. Look at the ParseException
                                            // to figure out what went wrong
                                        }
                                    }
                                }
        );

        edit_user_title.setText("");
        et_user_bio.setText("");
        et_user_location.setText("");
    }


}
