package edu.sjsu.android.heart2art;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.w3c.dom.Text;

public class UserProfile extends Fragment {
    final FragmentManager fragmentManager = getFragmentManager();
    private View view;
    Button photos_btn;
    Fragment photos_fragment;
    TextView user_name;
    TextView user_title;
    TextView user_bio;
    TextView user_location;
    TextView friend_amount;
    TextView photo_amount;
    Post post;
    de.hdodenhof.circleimageview.CircleImageView image;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        photos_btn = (Button)view.findViewById(R.id.photos_btn);
        photos_btn.setBackgroundResource(R.drawable.elem2_outline);
        user_name = (TextView)view.findViewById(R.id.user_name);
        user_title = (TextView)view.findViewById(R.id.user_title);
        user_bio = (TextView)view.findViewById(R.id.user_bio);
        user_location = (TextView)view.findViewById(R.id.user_location);
        image = (de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.circle_image);
        friend_amount = (TextView)view.findViewById(R.id.friend_amount);
        photo_amount = (TextView)view.findViewById(R.id.photo_amount);

        user_name.setText(ParseUser.getCurrentUser().getUsername());
        user_title.setText(ParseUser.getCurrentUser().getString("user_title"));
        user_bio.setText(ParseUser.getCurrentUser().getString("bio"));
        user_location.setText(ParseUser.getCurrentUser().getString("user_location"));

        loadProfilePic(ParseUser.getCurrentUser().getParseFile("user_photo"), image);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
        int count = 0;
        try {
            count = query.count();
        } catch (ParseException e) {
            count = 5;
            e.printStackTrace();
        }
        friend_amount.setText((count - 1) + "");

        //_User = ParseUser.getCurrentUser().getString("objectId")
        query = ParseQuery.getQuery("Dashboard");
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        count = 0;
        try {
            count = query.count();
        } catch (ParseException e) {
            count = 5;
            e.printStackTrace();
        }
        photo_amount.setText(count + "");

        photos_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photos_fragment = new ProfileFragment();
                replaceFragment(photos_fragment);
            }
        });
    }

    /**
     * This method is from the following Stack Overflow link: https://stackoverflow.com/questions/27682857/android-load-an-imageview-from-a-parsefile
     * @param thumbnail
     * @param img
     */
    private void loadProfilePic(ParseFile thumbnail, final ImageView img) {

        if (thumbnail != null) {
            thumbnail.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                        img.setImageBitmap(bmp);
                    } else {
                    }
                }
            });
        } else {
            img.setImageResource(R.drawable.doge);
        }
    }

    public void replaceFragment(Fragment someFragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.flContainer, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
